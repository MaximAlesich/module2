using System;
using System.Globalization;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return (companiesNumber * companyRevenue * tax) / 100;
        }

        public string GetCongratulation(int input)
        {

            if (input % 2 == 0 && input >= 18)
            {
                return "Поздравляю с совершеннолетием!";
            }

            else if (input % 2 != 0 && input < 18 && input > 12)
            {
                return "Поздравляю с переходом в старшую школу!";
            }

            else
            {
                return $"Поздравляю c {input}-летием!";
            }

        }

        public double GetMultipliedNumbers(string first, string second)
        {

            if (!double.TryParse(first, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out double numFirst))
            {
                throw new Exception();
            }

            if (!double.TryParse(second, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out double numSecond))
            {
                throw new Exception();
            }

            return numFirst * numSecond;
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            switch (figureType)
            {
                case FigureEnum.Triangle:
                    {
                        if (parameterToCompute == ParameterEnum.Perimeter)
                        {
                            if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0 && dimensions.ThirdSide > 0)
                            {
                                return Math.Round(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                            }
                        }
                        else if (parameterToCompute == ParameterEnum.Square)
                        {

                            if (dimensions.FirstSide > 0 && dimensions.Height > 0)
                            {
                                return Math.Round((dimensions.FirstSide * dimensions.Height) / 2);
                            }
                        }
                    }
                    break;
                case FigureEnum.Rectangle:
                    {
                        if (parameterToCompute == ParameterEnum.Perimeter)
                        {
                            if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0)
                            {
                                return Math.Round((dimensions.FirstSide * 2) + (dimensions.SecondSide * 2));
                            }
                            else if (parameterToCompute == ParameterEnum.Square)
                            {
                                if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0)
                                {
                                    return Math.Round(dimensions.FirstSide * dimensions.SecondSide);
                                }
                            }
                        }
                    }
                    break;
                case FigureEnum.Circle:
                    {
                        if (parameterToCompute == ParameterEnum.Square)
                        {
                            if (dimensions.Diameter > 0)
                            {
                                return Math.Round(Math.PI * Math.Pow(dimensions.Diameter / 2, 2));
                            }
                            else if (dimensions.Radius > 0)
                            {
                                return Math.Round(Math.PI * Math.Pow(dimensions.Radius, 2));
                            }
                        }
                    }
                    break;
            }
            throw new Exception();
        }
    }
}

